package katas.roman;

import org.junit.Assert;
import org.junit.Test;

public class RomanNumeralsTest {

	@Test
	public void arabicToRomanUnstandardValues() {
		try {
			RomanToArabicConverter.convertFrom("");
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("An empty string does not define a Roman numeral"))
				throw e;
		}

		String illigalSymbol = "Z";
		try {
			RomanToArabicConverter.convertFrom(illigalSymbol);
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("Illegal character " + illigalSymbol + " in roman numeral"))
				throw e;
		}

		try {
			RomanToArabicConverter.convertFrom("DD");
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("The symbols D, L, V can never be repeated"))
				throw e;
		}

		try {
			RomanToArabicConverter.convertFrom("IIII");
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("The symbols I, X, C, M can be repeated three times in succession for addition, but no more"))
				throw e;
		}

		try {
			RomanToArabicConverter.convertFrom("IM");
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("Given symbols cannt be subtracted"))
				throw e;
		}

		try {
			RomanToArabicConverter.convertFrom("IIV");
		} catch (NumberFormatException e) {
			if (!e.getMessage().equals("Symbols can not be repeated for subtraction"))
				throw e;
		}
	}

	@Test
	public void arabicToRoman() {
		accertRoman("i", 1);
		accertRoman("ii", 2);
		accertRoman("iii", 3);
		accertRoman("iv", 4);
		accertRoman("v", 5);
		accertRoman("ix", 9);
		accertRoman("x", 10);
		accertRoman("xl", 40);
		accertRoman("l", 50);
		accertRoman("xc", 90);
		accertRoman("c", 100);
		accertRoman("cd", 400);
		accertRoman("d", 500);
		accertRoman("cm", 900);
		accertRoman("m", 1000);
		accertRoman("mmxiii", 2013);
		accertRoman("cmxcix", 999);
		accertRoman("dclxvi", 666);
	}

	private void accertRoman(String roman, int arabic) {
		Assert.assertEquals(String.valueOf(arabic), arabic, RomanToArabicConverter.convertFrom(roman));
	}
}