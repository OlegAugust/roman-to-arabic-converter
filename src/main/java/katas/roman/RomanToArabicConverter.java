package katas.roman;

public class RomanToArabicConverter {

	private static final int I = 1;
	private static final int V = 5;
	private static final int X = 10;
	private static final int L = 50;
	private static final int C = 100;
	private static final int D = 500;
	private static final int M = 1000;

	public static int convertFrom(String roman) throws NumberFormatException {
		if (roman.length() == 0)
			throw new NumberFormatException("An empty string does not define a Roman numeral");

		roman = roman.toUpperCase();
		int i = 0; // A position in the string, roman;
		int arabic = 0;

		char previousSymbol = '\u0000';
		int previousSymbolRepeated = 0;

		while (i < roman.length()) {
			char symbol = roman.charAt(i); 					// Symbol at current position in string.
			int symbolNumber = symbolToNumber(symbol); 		// Numerical equivalent of symbol.
			if (symbolNumber < 0)
				throw new NumberFormatException("Illegal character " + symbol + " in roman numeral");

			if (symbol == previousSymbol) {
				++previousSymbolRepeated;
				if (previousSymbolRepeated == 2 && isNonRepeatedSymbol(symbolNumber))
					throw new NumberFormatException("The symbols D, L, V can never be repeated");

				if (previousSymbolRepeated == 4)
					throw new NumberFormatException("The symbols I, X, C, M can be repeated three times in succession for addition, but no more");
			} else {
				previousSymbol = symbol;
				previousSymbolRepeated = 1;
			}

			++i; // Move on to next position in the string
			if (i == roman.length()) {
				// There is no symbol in the string following the one we have just processed.
				// So just add the number corresponding to the single symbol to arabic.
				arabic += symbolNumber;
			} else {
				// Look at the next symbol in the string.  If it has a larger Roman numeral
                // equivalent than number, then the two symbols are counted together as
                // a Roman numeral with value (nextNumber - number).
				int nextNumber = symbolToNumber(roman.charAt(i));
				if (nextNumber > symbolNumber) {
					if (isCanSubtract(symbolNumber, nextNumber)) {
						// Combine the two symbols to get one value, and move on to next position in string.
						arabic += (nextNumber - symbolNumber);
						++i;
					} else {
						throw new NumberFormatException("Given symbols cannt be subtracted");
					}
				} else if (symbolNumber == nextNumber && (i + 1 < roman.length()) && symbolToNumber(roman.charAt(i + 1)) > symbolNumber) {
					throw new NumberFormatException("Symbols can not be repeated for subtraction");
				} else {
					// Don't combine the symbols. Just add the value of the one symbol onto the number.
					arabic += symbolNumber;
				}
			}
		}

		return arabic;
	}

	private static boolean isCanSubtract(int symbolNumber, int nextNumber) {
		if (symbolNumber == I && (nextNumber == V || nextNumber == X))
			return true;
		if (symbolNumber == X && (nextNumber == L || nextNumber == C))
			return true;
		if (symbolNumber == C && (nextNumber == D || nextNumber == M))
			return true;
		return false;
	}

	private static boolean isNonRepeatedSymbol(int number) {
		return (number == D || number == L || number == V) ? true : false;
	}

	private static int symbolToNumber(char letter) {
		switch (letter) {
		case 'I':
			return I;
		case 'V':
			return V;
		case 'X':
			return X;
		case 'L':
			return L;
		case 'C':
			return C;
		case 'D':
			return D;
		case 'M':
			return M;
		default:
			return -1;
		}
	}
}
